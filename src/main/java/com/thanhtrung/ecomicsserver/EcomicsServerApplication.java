package com.thanhtrung.ecomicsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcomicsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcomicsServerApplication.class, args);
    }

}
