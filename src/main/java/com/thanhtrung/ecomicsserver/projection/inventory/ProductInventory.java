package com.thanhtrung.ecomicsserver.projection.inventory;


import com.thanhtrung.ecomicsserver.entity.product.Product;
import lombok.Data;



@Data
public class ProductInventory {
    private Product product;
    private Integer inventory;
    private Integer waitingForDelivery;
    private Integer canBeSold;
    private Integer areComing;
}
