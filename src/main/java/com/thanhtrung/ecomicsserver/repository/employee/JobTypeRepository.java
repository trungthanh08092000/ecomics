package com.thanhtrung.ecomicsserver.repository.employee;

import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface JobTypeRepository extends JpaRepository<JobType, Long>, JpaSpecificationExecutor<JobType> {
    Optional<JobType> findById(Long Id);
}
