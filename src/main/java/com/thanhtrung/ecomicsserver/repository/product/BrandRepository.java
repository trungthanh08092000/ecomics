package com.thanhtrung.ecomicsserver.repository.product;


import com.thanhtrung.ecomicsserver.entity.product.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface BrandRepository extends JpaRepository<Brand, Long>, JpaSpecificationExecutor<Brand> {

    Optional<Brand> findById(Long Id);

    @Query("SELECT COUNT(b.id) FROM Brand b")
    int countByBrandId();

}
