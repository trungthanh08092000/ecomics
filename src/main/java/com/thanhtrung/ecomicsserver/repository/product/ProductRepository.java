package com.thanhtrung.ecomicsserver.repository.product;


import com.thanhtrung.ecomicsserver.entity.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {


    Optional<Product> findBySlug(String slug);


    @Query("SELECT COUNT(p.id) FROM Product p")
    int countByProductId();

}
