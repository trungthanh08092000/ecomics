package com.thanhtrung.ecomicsserver.repository.authentication;

import com.thanhtrung.ecomicsserver.entity.authentication.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {
}
