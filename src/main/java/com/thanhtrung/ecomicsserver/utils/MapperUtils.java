package com.thanhtrung.ecomicsserver.utils;

import com.thanhtrung.ecomicsserver.entity.BaseEntity;
import com.thanhtrung.ecomicsserver.entity.address.District;
import com.thanhtrung.ecomicsserver.entity.address.Province;
import com.thanhtrung.ecomicsserver.entity.address.Ward;
import com.thanhtrung.ecomicsserver.entity.authentication.User;
import com.thanhtrung.ecomicsserver.entity.chat.Room;
import com.thanhtrung.ecomicsserver.entity.customer.Customer;
import com.thanhtrung.ecomicsserver.entity.customer.CustomerGroup;
import com.thanhtrung.ecomicsserver.entity.customer.CustomerResource;
import com.thanhtrung.ecomicsserver.entity.customer.CustomerStatus;
import com.thanhtrung.ecomicsserver.entity.employee.*;
import com.thanhtrung.ecomicsserver.entity.inventory.*;
import com.thanhtrung.ecomicsserver.entity.product.*;
import com.thanhtrung.ecomicsserver.repository.address.DistrictRepository;
import com.thanhtrung.ecomicsserver.repository.address.ProvinceRepository;
import com.thanhtrung.ecomicsserver.repository.address.WardRepository;
import com.thanhtrung.ecomicsserver.repository.authentication.RoleRepository;
import com.thanhtrung.ecomicsserver.repository.authentication.UserRepository;
import com.thanhtrung.ecomicsserver.repository.product.ProductRepository;
import com.thanhtrung.ecomicsserver.repository.product.TagRepository;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class MapperUtils {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProvinceRepository provinceRepository;
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private WardRepository wardRepository;
    @Autowired
    private UserRepository userRepository;

    public Province mapToProvince(@Nullable Long id) {
        return id == null ? null : provinceRepository.getById(id);
    }

    public District mapToDistrict(@Nullable Long id) {
        return id == null ? null : districtRepository.getById(id);
    }

    public Ward mapToWard(@Nullable Long id) {
        return id == null ? null : wardRepository.getById(id);
    }

    public abstract Office mapToOffice(Long id);

    public abstract Department mapToDepartment(Long id);

    public abstract JobType mapToJobType(Long id);

    public abstract JobLevel mapToJobLevel(Long id);

    public abstract JobTitle mapToJobTitle(Long id);

    public abstract CustomerGroup mapToCustomerGroup(Long id);

    public abstract CustomerResource mapToCustomerResource(Long id);

    public abstract CustomerStatus mapToCustomerStatus(Long id);

    public abstract Category mapToCategory(Long id);

    public abstract Brand mapToBrand(Long id);

    public abstract Supplier mapToSupplier(Long id);

    public abstract Unit mapToUnit(Long id);

    public abstract Warehouse mapToWarehouse(Long id);


    public abstract Customer mapToCustomer(Long id);


    public abstract Room mapToRoom(Long id);

    public Product mapToProduct(Long id) {
        return productRepository.getById(id);
    }

    public User mapToUser(Long id) {
        return userRepository.getById(id);
    }

    @Named("hashPassword")
    public String hashPassword(String password) {
        return passwordEncoder.encode(password);
    }

    @AfterMapping
    @Named("attachUser")
    public User attachUser(@MappingTarget User user) {
        return user.setRoles(attachSet(user.getRoles(), roleRepository));
    }

    @AfterMapping
    @Named("attachProduct")
    public Product attachProduct(@MappingTarget Product product) {
        product.getImages().forEach(image -> image.setProduct(product));
        product.setTags(attachSet(product.getTags(), tagRepository));
        return product;
    }

    private <E extends BaseEntity> Set<E> attachSet(Set<E> entities, JpaRepository<E, Long> repository) {
        Set<E> detachedSet = Optional.ofNullable(entities).orElseGet(HashSet::new);
        Set<E> attachedSet = new HashSet<>();

        for (E entity : detachedSet) {
            if (entity.getId() != null) {
                repository.findById(entity.getId()).ifPresent(attachedSet::add);
            } else {
                attachedSet.add(entity);
            }
        }

        return attachedSet;
    }

}
