package com.thanhtrung.ecomicsserver.dto.employee;

import lombok.Data;

@Data
public class JobLevelRequest {
    private String name;
    private Integer status;
}
