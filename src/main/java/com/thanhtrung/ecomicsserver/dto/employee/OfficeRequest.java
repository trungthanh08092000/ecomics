package com.thanhtrung.ecomicsserver.dto.employee;

import com.thanhtrung.ecomicsserver.dto.address.AddressRequest;
import lombok.Data;

@Data
public class OfficeRequest {
    private String name;
    private AddressRequest address;
    private Integer status;
}
