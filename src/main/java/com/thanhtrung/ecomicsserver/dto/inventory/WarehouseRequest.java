package com.thanhtrung.ecomicsserver.dto.inventory;

import com.thanhtrung.ecomicsserver.dto.address.AddressRequest;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class WarehouseRequest {
    private String code;
    private String name;
    @Nullable
    private AddressRequest address;
    private Integer status;
}
