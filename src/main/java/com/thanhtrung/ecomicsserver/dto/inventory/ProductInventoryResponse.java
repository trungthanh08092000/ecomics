package com.thanhtrung.ecomicsserver.dto.inventory;


import com.thanhtrung.ecomicsserver.dto.product.BrandResponse;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.time.Instant;

@Data
public class ProductInventoryResponse {
    private ProductResponse product;
    private Integer inventory;
    private Integer waitingForDelivery;
    private Integer canBeSold;
    private Integer areComing;

    @Data
    public static class ProductResponse {
        private Long id;
        private Instant createdAt;
        private Instant updatedAt;
        private String name;
        private String code;
        private String slug;
        @Nullable
        private BrandResponse brand;
    }
}
