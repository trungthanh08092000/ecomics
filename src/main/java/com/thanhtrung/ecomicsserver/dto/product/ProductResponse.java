package com.thanhtrung.ecomicsserver.dto.product;

import com.thanhtrung.ecomicsserver.dto.general.ImageResponse;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
public class ProductResponse {
    private Long id;
    private Instant createdAt;
    private Instant updatedAt;
    private String name;
    private String code;
    private String slug;
    @Nullable
    private String shortDescription;
    @Nullable
    private String description;
    private List<ImageResponse> images;
    private Integer status;
    @Nullable
    private CategoryResponse category;
    @Nullable
    private BrandResponse brand;
    @Nullable
    private SupplierResponse supplier;
    @Nullable
    private UnitResponse unit;
    private Set<TagResponse> tags;

    @Data
    public static class CategoryResponse {
        private Long id;
        private Instant createdAt;
        private Instant updatedAt;
        private String name;
        private String slug;
        @Nullable
        private String description;
        @Nullable
        private String thumbnail;
        private Integer status;
    }
}
