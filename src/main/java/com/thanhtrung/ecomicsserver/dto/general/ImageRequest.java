package com.thanhtrung.ecomicsserver.dto.general;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Data
public class ImageRequest {
    private @Nullable Long id;
    @Getter
    private String name;
    @Getter
    private String path;
    @Getter
    private String contentType;
    @Getter
    private Long size;
    @Getter
    private String group;
    private Boolean isThumbnail;
    private Boolean isEliminated;

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Boolean getThumbnail() {
        return isThumbnail;
    }

    public void setThumbnail(Boolean thumbnail) {
        isThumbnail = thumbnail;
    }

    public Boolean getEliminated() {
        return isEliminated;
    }

    public void setEliminated(Boolean eliminated) {
        isEliminated = eliminated;
    }
}
