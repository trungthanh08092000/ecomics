package com.thanhtrung.ecomicsserver.mapper.promotion;

import com.thanhtrung.ecomicsserver.dto.promotion.PromotionRequest;
import com.thanhtrung.ecomicsserver.dto.promotion.PromotionResponse;
import com.thanhtrung.ecomicsserver.entity.product.Category;
import com.thanhtrung.ecomicsserver.entity.product.Product;
import com.thanhtrung.ecomicsserver.entity.promotion.Promotion;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.mapper.product.ProductMapper;
import com.thanhtrung.ecomicsserver.repository.product.CategoryRepository;
import com.thanhtrung.ecomicsserver.utils.MapperUtils;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {MapperUtils.class, ProductMapper.class})
public abstract class PromotionMapper implements GenericMapper<Promotion, PromotionRequest, PromotionResponse> {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    @BeanMapping(qualifiedByName = "addProductsFromCategories")
    @Mapping(source = "productIds", target = "products")
    public abstract Promotion requestToEntity(PromotionRequest request);

    @Override
    @Mapping(source = "productIds", target = "products")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract Promotion partialUpdate(@MappingTarget Promotion entity, PromotionRequest request);

    @AfterMapping
    @Named("addProductsFromCategories")
    protected void addProductsFromCategories(@MappingTarget Promotion promotion, PromotionRequest request) {
        if (request.getCategoryIds().size() != 0) {
            Set<Product> productsFromCategories = request.getCategoryIds().stream()
                    .map(categoryRepository::getById)
                    .map(Category::getProducts)
                    .flatMap(List::stream)
                    .collect(Collectors.toSet());

            promotion.setProducts(productsFromCategories);
        }
    }


}
