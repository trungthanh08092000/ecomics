package com.thanhtrung.ecomicsserver.mapper.general;

import com.thanhtrung.ecomicsserver.dto.general.ImageRequest;
import com.thanhtrung.ecomicsserver.dto.general.ImageResponse;
import com.thanhtrung.ecomicsserver.entity.general.Image;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ImageMapper extends GenericMapper<Image, ImageRequest, ImageResponse> {}
