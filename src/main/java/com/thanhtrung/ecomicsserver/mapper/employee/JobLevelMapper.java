package com.thanhtrung.ecomicsserver.mapper.employee;

import com.thanhtrung.ecomicsserver.dto.employee.JobLevelRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobLevel;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JobLevelMapper extends GenericMapper<JobLevel, JobLevelRequest, JobLevelResponse> {
}
