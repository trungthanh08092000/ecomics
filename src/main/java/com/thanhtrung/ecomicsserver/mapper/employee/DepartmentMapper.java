package com.thanhtrung.ecomicsserver.mapper.employee;

import com.thanhtrung.ecomicsserver.dto.employee.DepartmentRequest;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentResponse;
import com.thanhtrung.ecomicsserver.entity.employee.Department;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DepartmentMapper extends GenericMapper<Department, DepartmentRequest, DepartmentResponse> {
}
