package com.thanhtrung.ecomicsserver.mapper.employee;

import com.thanhtrung.ecomicsserver.dto.employee.OfficeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeResponse;
import com.thanhtrung.ecomicsserver.entity.employee.Office;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.mapper.address.AddressMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = AddressMapper.class)
public interface OfficeMapper extends GenericMapper<Office, OfficeRequest, OfficeResponse> {
}
