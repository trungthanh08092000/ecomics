package com.thanhtrung.ecomicsserver.mapper.employee;

import com.thanhtrung.ecomicsserver.dto.employee.JobTypeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface JobTypeMapper extends GenericMapper<JobType, JobTypeRequest, JobTypeResponse> {
}
