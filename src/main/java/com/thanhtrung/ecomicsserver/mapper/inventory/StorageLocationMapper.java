package com.thanhtrung.ecomicsserver.mapper.inventory;

import com.thanhtrung.ecomicsserver.dto.inventory.StorageLocationRequest;
import com.thanhtrung.ecomicsserver.dto.inventory.StorageLocationResponse;
import com.thanhtrung.ecomicsserver.entity.inventory.StorageLocation;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.utils.MapperUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {MapperUtils.class, WarehouseMapper.class})
public interface StorageLocationMapper extends GenericMapper<StorageLocation, StorageLocationRequest, StorageLocationResponse> {

    @Override
    @Mapping(source = "warehouseId", target = "warehouse")
    StorageLocation requestToEntity(StorageLocationRequest request);

    @Override
    @Mapping(source = "warehouseId", target = "warehouse")
    StorageLocation partialUpdate(@MappingTarget StorageLocation entity, StorageLocationRequest request);

}
