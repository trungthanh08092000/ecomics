package com.thanhtrung.ecomicsserver.mapper.inventory;

import com.thanhtrung.ecomicsserver.dto.inventory.WarehouseRequest;
import com.thanhtrung.ecomicsserver.dto.inventory.WarehouseResponse;
import com.thanhtrung.ecomicsserver.entity.inventory.Warehouse;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.mapper.address.AddressMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = AddressMapper.class)
public interface WarehouseMapper extends GenericMapper<Warehouse, WarehouseRequest, WarehouseResponse> {}
