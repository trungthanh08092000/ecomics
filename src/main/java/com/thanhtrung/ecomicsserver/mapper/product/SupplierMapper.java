package com.thanhtrung.ecomicsserver.mapper.product;

import com.thanhtrung.ecomicsserver.dto.product.SupplierRequest;
import com.thanhtrung.ecomicsserver.dto.product.SupplierResponse;
import com.thanhtrung.ecomicsserver.entity.product.Supplier;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.mapper.address.AddressMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = AddressMapper.class)
public interface SupplierMapper extends GenericMapper<Supplier, SupplierRequest, SupplierResponse> {
}
