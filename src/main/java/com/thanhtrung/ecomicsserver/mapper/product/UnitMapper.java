package com.thanhtrung.ecomicsserver.mapper.product;

import com.thanhtrung.ecomicsserver.dto.product.UnitRequest;
import com.thanhtrung.ecomicsserver.dto.product.UnitResponse;
import com.thanhtrung.ecomicsserver.entity.product.Unit;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UnitMapper extends GenericMapper<Unit, UnitRequest, UnitResponse> {
}
