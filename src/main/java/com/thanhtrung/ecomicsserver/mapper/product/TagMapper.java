package com.thanhtrung.ecomicsserver.mapper.product;

import com.thanhtrung.ecomicsserver.dto.product.TagRequest;
import com.thanhtrung.ecomicsserver.dto.product.TagResponse;
import com.thanhtrung.ecomicsserver.entity.product.Tag;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TagMapper extends GenericMapper<Tag, TagRequest, TagResponse> {
}
