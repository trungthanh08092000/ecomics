package com.thanhtrung.ecomicsserver.mapper.product;

import com.thanhtrung.ecomicsserver.dto.inventory.ProductInventoryResponse;
import com.thanhtrung.ecomicsserver.projection.inventory.ProductInventory;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductInventoryMapper {

    ProductInventoryResponse toResponse(ProductInventory productInventory);

    List<ProductInventoryResponse> toResponse(List<ProductInventory> productInventory);

}
