package com.thanhtrung.ecomicsserver.mapper.authentication;

import com.thanhtrung.ecomicsserver.dto.authentication.UserRequest;
import com.thanhtrung.ecomicsserver.dto.authentication.UserResponse;
import com.thanhtrung.ecomicsserver.entity.authentication.User;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import com.thanhtrung.ecomicsserver.mapper.address.AddressMapper;
import com.thanhtrung.ecomicsserver.utils.MapperUtils;
import org.mapstruct.*;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {MapperUtils.class, AddressMapper.class})
public interface UserMapper extends GenericMapper<User, UserRequest, UserResponse> {

    @Override
    @BeanMapping(qualifiedByName = "attachUser")
    @Mapping(source = "password", target = "password", qualifiedByName = "hashPassword")
    User requestToEntity(UserRequest request);

    @Override
    @BeanMapping(qualifiedByName = "attachUser")
    @Mapping(source = "password", target = "password", qualifiedByName = "hashPassword",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User partialUpdate(@MappingTarget User entity, UserRequest request);


}
