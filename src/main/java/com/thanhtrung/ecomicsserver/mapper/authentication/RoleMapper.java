package com.thanhtrung.ecomicsserver.mapper.authentication;

import com.thanhtrung.ecomicsserver.dto.authentication.RoleRequest;
import com.thanhtrung.ecomicsserver.dto.authentication.RoleResponse;
import com.thanhtrung.ecomicsserver.entity.authentication.Role;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleMapper extends GenericMapper<Role, RoleRequest, RoleResponse> {
}
