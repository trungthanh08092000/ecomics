package com.thanhtrung.ecomicsserver.mapper.address;
import com.thanhtrung.ecomicsserver.dto.address.ProvinceRequest;
import com.thanhtrung.ecomicsserver.dto.address.ProvinceResponse;
import com.thanhtrung.ecomicsserver.entity.address.Province;
import com.thanhtrung.ecomicsserver.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProvinceMapper extends GenericMapper<Province, ProvinceRequest, ProvinceResponse> {
}
