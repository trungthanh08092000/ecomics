package com.thanhtrung.ecomicsserver.entity.authentication;

public enum VerificationType {
    REGISTRATION,
    FORGET_PASSWORD
}
