package com.thanhtrung.ecomicsserver.entity.general;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.thanhtrung.ecomicsserver.entity.BaseEntity;
import com.thanhtrung.ecomicsserver.entity.product.Product;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "image")
public class Image extends BaseEntity {
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "path", nullable = false, unique = true)
    private String path;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(name = "size", nullable = false)
    private Long size;

    /**
     * Có 3 nhóm (group):
     * <ul>
     *     <li>P – Product</li>
     *     <li>C – Category</li>
     *     <li>U – User</li>
     * </ul>
     */
    @Column(name = "`group`", nullable = false)
    private String group;

    @Column(name = "is_thumbnail", nullable = false, columnDefinition = "BOOLEAN")
    private Boolean isThumbnail;

    @Column(name = "is_eliminated", nullable = false, columnDefinition = "BOOLEAN")
    private Boolean isEliminated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @JsonBackReference
    private Product product;

    public Image(String name, String path, String contentType, Long size, String group, Boolean isThumbnail, Boolean isEliminated) {
        this.name = name;
        this.path = path;
        this.contentType = contentType;
        this.size = size;
        this.group = group;
        this.isThumbnail = isThumbnail;
        this.isEliminated = isEliminated;
    }
}
