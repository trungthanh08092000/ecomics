package com.thanhtrung.ecomicsserver.controller.general;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.general.ImageRequest;
import com.thanhtrung.ecomicsserver.dto.general.ImageResponse;
import com.thanhtrung.ecomicsserver.service.general.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/images")
public class ImageController {
    @Autowired
    private ImageService imageService ;

    @PostMapping
    public ResponseEntity<ImageResponse> createBrand(@RequestBody ImageRequest imageRequest) {
        ImageResponse brandResponse = imageService.create(imageRequest);
        return ResponseEntity.ok(brandResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ImageResponse> updateBrand(@PathVariable Long id, @RequestBody ImageRequest imageRequest) {
        ImageResponse brandResponse = imageService.save(id, imageRequest);
        return ResponseEntity.ok(brandResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBrand(@PathVariable Long id) {
        imageService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteBrands(@RequestBody List<Long> ids) {
        imageService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ImageResponse> getBrandById(@PathVariable Long id) {
        ImageResponse imageResponse = imageService.findById(id);
        return ResponseEntity.ok(imageResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<ImageResponse>> searchBrands(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<ImageResponse> imageResponseListResponse = imageService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(imageResponseListResponse);
    }
}
