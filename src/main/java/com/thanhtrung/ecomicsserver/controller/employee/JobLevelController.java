package com.thanhtrung.ecomicsserver.controller.employee;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelResponse;
import com.thanhtrung.ecomicsserver.service.employee.JobLevelService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/jobLevels")
@AllArgsConstructor
public class JobLevelController {
    @Autowired
    private JobLevelService jobLevelService;

    @PostMapping
    public ResponseEntity<JobLevelResponse> createJobLevel(@RequestBody JobLevelRequest jobLevelRequest) {
        JobLevelResponse jobLevelResponse = jobLevelService.create(jobLevelRequest);
        return ResponseEntity.ok(jobLevelResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JobLevelResponse> updateJobLevel(@PathVariable Long id, @RequestBody JobLevelRequest jobLevelRequest) {
        JobLevelResponse jobLevelResponse = jobLevelService.save(id, jobLevelRequest);
        return ResponseEntity.ok(jobLevelResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteJobLevel(@PathVariable Long id) {
        jobLevelService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteJobLevels(@RequestBody List<Long> ids) {
        jobLevelService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<JobLevelResponse> getJobLevelById(@PathVariable Long id) {
        JobLevelResponse jobLevelResponse = jobLevelService.findById(id);
        return ResponseEntity.ok(jobLevelResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<JobLevelResponse>> searchJobLevels(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<JobLevelResponse> jobLevelResponseListResponse = jobLevelService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(jobLevelResponseListResponse);
    }
}
