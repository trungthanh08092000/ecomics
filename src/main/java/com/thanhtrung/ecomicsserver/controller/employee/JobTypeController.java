package com.thanhtrung.ecomicsserver.controller.employee;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeResponse;
import com.thanhtrung.ecomicsserver.service.employee.JobTypeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/jobTypes")
@AllArgsConstructor
public class JobTypeController {
    @Autowired
    private JobTypeService jobTypeService;

    @PostMapping
    public ResponseEntity<JobTypeResponse> createJobType(@RequestBody JobTypeRequest jobTypeRequest) {
        JobTypeResponse jobTypeResponse = jobTypeService.create(jobTypeRequest);
        return ResponseEntity.ok(jobTypeResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JobTypeResponse> updateJobType(@PathVariable Long id, @RequestBody JobTypeRequest jobTypeRequest) {
        JobTypeResponse jobTypeResponse = jobTypeService.save(id, jobTypeRequest);
        return ResponseEntity.ok(jobTypeResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteJobType(@PathVariable Long id) {
        jobTypeService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteJobTypes(@RequestBody List<Long> ids) {
        jobTypeService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<JobTypeResponse> getJobTypeById(@PathVariable Long id) {
        JobTypeResponse jobTypeResponse = jobTypeService.findById(id);
        return ResponseEntity.ok(jobTypeResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<JobTypeResponse>> searchJobTypes(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<JobTypeResponse> jobTypeResponseListResponse = jobTypeService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(jobTypeResponseListResponse);
    }
}
