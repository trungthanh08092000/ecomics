package com.thanhtrung.ecomicsserver.controller.employee;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleResponse;
import com.thanhtrung.ecomicsserver.service.employee.JobTitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/jobTiles")
public class JobTitleController {
    @Autowired
    private JobTitleService jobTitleService;

    @PostMapping
    public ResponseEntity<JobTitleResponse> createJobTitle(@RequestBody JobTitleRequest jobTitleRequest) {
        JobTitleResponse jobTitleResponse = jobTitleService.create(jobTitleRequest);
        return ResponseEntity.ok(jobTitleResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<JobTitleResponse> updateJobTitle(@PathVariable Long id, @RequestBody JobTitleRequest jobTitleRequest) {
        JobTitleResponse jobTitleResponse = jobTitleService.save(id, jobTitleRequest);
        return ResponseEntity.ok(jobTitleResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteJobTitle(@PathVariable Long id) {
        jobTitleService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteJobTitles(@RequestBody List<Long> ids) {
        jobTitleService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<JobTitleResponse> getJobTitleById(@PathVariable Long id) {
        JobTitleResponse jobTitleResponse = jobTitleService.findById(id);
        return ResponseEntity.ok(jobTitleResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<JobTitleResponse>> searchJobTitles(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<JobTitleResponse> jobTitleResponseListResponse = jobTitleService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(jobTitleResponseListResponse);
    }
}
