package com.thanhtrung.ecomicsserver.controller.employee;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeResponse;
import com.thanhtrung.ecomicsserver.service.employee.OfficeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/offices")
@AllArgsConstructor
public class OfficeController {
    @Autowired
    private OfficeService officeService;

    @PostMapping
    public ResponseEntity<OfficeResponse> createBrand(@RequestBody OfficeRequest officeRequest) {
        OfficeResponse officeResponse = officeService.create(officeRequest);
        return ResponseEntity.ok(officeResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OfficeResponse> updateBrand(@PathVariable Long id, @RequestBody OfficeRequest officeRequest) {
        OfficeResponse officeResponse = officeService.save(id, officeRequest);
        return ResponseEntity.ok(officeResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBrand(@PathVariable Long id) {
        officeService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteBrands(@RequestBody List<Long> ids) {
        officeService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<OfficeResponse> getBrandById(@PathVariable Long id) {
        OfficeResponse officeResponse = officeService.findById(id);
        return ResponseEntity.ok(officeResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<OfficeResponse>> searchBrands(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<OfficeResponse> officeResponseListResponse = officeService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(officeResponseListResponse);
    }
}
