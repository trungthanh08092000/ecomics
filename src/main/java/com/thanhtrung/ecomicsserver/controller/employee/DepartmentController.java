package com.thanhtrung.ecomicsserver.controller.employee;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentRequest;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentResponse;
import com.thanhtrung.ecomicsserver.service.employee.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/departments")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @PostMapping
    public ResponseEntity<DepartmentResponse> createDepartment(@RequestBody DepartmentRequest departmentRequest) {
        DepartmentResponse departmentResponse = departmentService.create(departmentRequest);
        return ResponseEntity.ok(departmentResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DepartmentResponse> updateDepartment(@PathVariable Long id, @RequestBody DepartmentRequest departmentRequest) {
        DepartmentResponse departmentResponse = departmentService.save(id, departmentRequest);
        return ResponseEntity.ok(departmentResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
        departmentService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteDepartments(@RequestBody List<Long> ids) {
        departmentService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<DepartmentResponse> getDepartmentById(@PathVariable Long id) {
        DepartmentResponse brandResponse = departmentService.findById(id);
        return ResponseEntity.ok(brandResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<DepartmentResponse>> searchDepartments(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<DepartmentResponse> departmentResponseListResponse = departmentService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(departmentResponseListResponse);
    }
}
