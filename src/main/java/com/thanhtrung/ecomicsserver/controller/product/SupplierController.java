package com.thanhtrung.ecomicsserver.controller.product;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.SupplierRequest;
import com.thanhtrung.ecomicsserver.dto.product.SupplierResponse;
import com.thanhtrung.ecomicsserver.service.production.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/suppliers")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @PostMapping
    public ResponseEntity<SupplierResponse> createSupplier(@RequestBody SupplierRequest supplierRequest) {
        SupplierResponse supplierResponse = supplierService.create(supplierRequest);
        return ResponseEntity.ok(supplierResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SupplierResponse> updateSupplier(@PathVariable Long id, @RequestBody SupplierRequest supplierRequest) {
        SupplierResponse supplierResponse = supplierService.save(id, supplierRequest);
        return ResponseEntity.ok(supplierResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSupplier(@PathVariable Long id) {
        supplierService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteSuppliers(@RequestBody List<Long> ids) {
        supplierService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplierResponse> getSupplierById(@PathVariable Long id) {
        SupplierResponse brandResponse = supplierService.findById(id);
        return ResponseEntity.ok(brandResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<SupplierResponse>> searchSuppliers(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<SupplierResponse> categoryResponseListResponse = supplierService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(categoryResponseListResponse);
    }
}
