package com.thanhtrung.ecomicsserver.controller.product;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.CategoryRequest;
import com.thanhtrung.ecomicsserver.dto.product.CategoryResponse;
import com.thanhtrung.ecomicsserver.service.production.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category's")
@AllArgsConstructor
public class CategoryController {
        @Autowired
        private CategoryService categoryservice;

        @PostMapping
        public ResponseEntity<CategoryResponse> createCateGory(@RequestBody CategoryRequest categoryRequest) {
            CategoryResponse categoryResponse = categoryservice.create(categoryRequest);
            return ResponseEntity.ok(categoryResponse);
        }

        @PutMapping("/{id}")
        public ResponseEntity<CategoryResponse> updateCateGory(@PathVariable Long id, @RequestBody CategoryRequest categoryRequest) {
            CategoryResponse categoryResponse = categoryservice.save(id, categoryRequest);
            return ResponseEntity.ok(categoryResponse);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<Void> deleteCateGory(@PathVariable Long id) {
            categoryservice.delete(id);
            return ResponseEntity.noContent().build();
        }

        @DeleteMapping
        public ResponseEntity<Void> deleteCateGorys(@RequestBody List<Long> ids) {
            categoryservice.delete(ids);
            return ResponseEntity.noContent().build();
        }

        @GetMapping("/{id}")
        public ResponseEntity<CategoryResponse> getCateGoryById(@PathVariable Long id) {
            CategoryResponse brandResponse = categoryservice.findById(id);
            return ResponseEntity.ok(brandResponse);
        }

        @GetMapping("/search")
        public ResponseEntity<ListResponse<CategoryResponse>> searchCategorys(
                @RequestParam int page,
                @RequestParam int size,
                @RequestParam(required = false) String sort,
                @RequestParam(required = false) String filter,
                @RequestParam(required = false) String search,
                @RequestParam(defaultValue = "false") boolean all) {
            ListResponse<CategoryResponse> categoryResponseListResponse = categoryservice.findAll(page, size, sort, filter, search, all);
            return ResponseEntity.ok(categoryResponseListResponse);
        }
}
