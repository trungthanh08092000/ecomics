package com.thanhtrung.ecomicsserver.controller.product;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.BrandRequest;
import com.thanhtrung.ecomicsserver.dto.product.BrandResponse;
import com.thanhtrung.ecomicsserver.service.production.BrandService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/brands")
@AllArgsConstructor
public class BrandController {
    @Autowired
    private BrandService brandService;

    @PostMapping
    public ResponseEntity<BrandResponse> createBrand(@RequestBody BrandRequest brandRequest) {
        BrandResponse brandResponse = brandService.create(brandRequest);
        return ResponseEntity.ok(brandResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BrandResponse> updateBrand(@PathVariable Long id, @RequestBody BrandRequest brandRequest) {
        BrandResponse brandResponse = brandService.save(id, brandRequest);
        return ResponseEntity.ok(brandResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBrand(@PathVariable Long id) {
        brandService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteBrands(@RequestBody List<Long> ids) {
        brandService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BrandResponse> getBrandById(@PathVariable Long id) {
        BrandResponse brandResponse = brandService.findById(id);
        return ResponseEntity.ok(brandResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<BrandResponse>> searchBrands(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<BrandResponse> brandResponses = brandService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(brandResponses);
    }
}
