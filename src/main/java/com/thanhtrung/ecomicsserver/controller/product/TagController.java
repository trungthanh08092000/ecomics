package com.thanhtrung.ecomicsserver.controller.product;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.TagRequest;
import com.thanhtrung.ecomicsserver.dto.product.TagResponse;
import com.thanhtrung.ecomicsserver.service.production.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagController {
    @Autowired
    private TagService tagService;

    @PostMapping
    public ResponseEntity<TagResponse> createTag(@RequestBody TagRequest tagRequest) {
        TagResponse tagResponse = tagService.create(tagRequest);
        return ResponseEntity.ok(tagResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TagResponse> updateTag(@PathVariable Long id, @RequestBody TagRequest tagRequest) {
        TagResponse tagResponse = tagService.save(id, tagRequest);
        return ResponseEntity.ok(tagResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTag(@PathVariable Long id) {
        tagService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteTags(@RequestBody List<Long> ids) {
        tagService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagResponse> getTagById(@PathVariable Long id) {
        TagResponse tagResponse = tagService.findById(id);
        return ResponseEntity.ok(tagResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<TagResponse>> searchTags(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<TagResponse> tagResponseListResponse = tagService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(tagResponseListResponse);
    }
}
