package com.thanhtrung.ecomicsserver.controller.product;

import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.SupplierRequest;
import com.thanhtrung.ecomicsserver.dto.product.SupplierResponse;
import com.thanhtrung.ecomicsserver.dto.product.UnitRequest;
import com.thanhtrung.ecomicsserver.dto.product.UnitResponse;
import com.thanhtrung.ecomicsserver.service.production.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/units")
public class UnitController {
    @Autowired
    private UnitService unitService;

    @PostMapping
    public ResponseEntity<UnitResponse> createUnit(@RequestBody UnitRequest unitRequest) {
        UnitResponse unitResponse = unitService.create(unitRequest);
        return ResponseEntity.ok(unitResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UnitResponse> updateUnit(@PathVariable Long id, @RequestBody UnitRequest unitRequest) {
        UnitResponse unitResponse = unitService.save(id, unitRequest);
        return ResponseEntity.ok(unitResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUnit(@PathVariable Long id) {
        unitService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteUnits(@RequestBody List<Long> ids) {
        unitService.delete(ids);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UnitResponse> getUnitById(@PathVariable Long id) {
        UnitResponse unitResponse = unitService.findById(id);
        return ResponseEntity.ok(unitResponse);
    }

    @GetMapping("/search")
    public ResponseEntity<ListResponse<UnitResponse>> searchSuppliers(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String search,
            @RequestParam(defaultValue = "false") boolean all) {
        ListResponse<UnitResponse> unitResponseListResponse = unitService.findAll(page, size, sort, filter, search, all);
        return ResponseEntity.ok(unitResponseListResponse);
    }
}
