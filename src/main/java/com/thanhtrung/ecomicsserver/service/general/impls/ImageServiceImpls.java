package com.thanhtrung.ecomicsserver.service.general.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.general.ImageRequest;
import com.thanhtrung.ecomicsserver.dto.general.ImageResponse;
import com.thanhtrung.ecomicsserver.entity.general.Image;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.general.ImageMapper;
import com.thanhtrung.ecomicsserver.repository.general.ImageRepository;
import com.thanhtrung.ecomicsserver.service.general.ImageService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class ImageServiceImpls implements ImageService {
    @Autowired
    private ImageRepository imageRepository;
    private ImageMapper imageMapper;
    @Override
    public ListResponse<ImageResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.IMAGE,imageRepository,imageMapper);
    }

    @Override
    public ImageResponse findById(Long id) {
        return defaultFindById(id,imageRepository,imageMapper, ResourceName.IMAGE);
    }

    @Override
    public ImageResponse create(ImageRequest request) {
        Image image = imageMapper.requestToEntity(request);
        return imageMapper.entityToResponse(imageRepository.save(image));
    }

    @Override
    public ImageResponse save(Long id, ImageRequest request) {
        Image image = imageRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        imageMapper.partialUpdate(image, request);
        return imageMapper.entityToResponse(imageRepository.save(image));
    }

    @Override
    public void delete(Long id) {
        if (!imageRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.IMAGE, FieldName.ID, id);
        }
        imageRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Image> images = ids.stream()
                .map(id -> imageRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        imageRepository.deleteAll(images);
    }
}
