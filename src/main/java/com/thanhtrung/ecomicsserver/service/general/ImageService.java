package com.thanhtrung.ecomicsserver.service.general;

import com.thanhtrung.ecomicsserver.dto.general.ImageRequest;
import com.thanhtrung.ecomicsserver.dto.general.ImageResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface ImageService extends CrudService<Long, ImageRequest, ImageResponse> {
}
