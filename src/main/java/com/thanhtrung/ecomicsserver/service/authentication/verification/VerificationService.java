package com.thanhtrung.ecomicsserver.service.authentication.verification;

import com.thanhtrung.ecomicsserver.dto.authentication.RegistrationRequest;
import com.thanhtrung.ecomicsserver.dto.authentication.ResetPasswordRequest;
import com.thanhtrung.ecomicsserver.dto.authentication.UserRequest;

public interface VerificationService {

    Long generateTokenVerify(UserRequest userRequest);

    void resendRegistrationToken(Long userId);

    void confirmRegistration(RegistrationRequest registration);

    void changeRegistrationEmail(Long userId, String emailUpdate);

    void forgetPassword(String email);

    void resetPassword(ResetPasswordRequest resetPasswordRequest);

}
