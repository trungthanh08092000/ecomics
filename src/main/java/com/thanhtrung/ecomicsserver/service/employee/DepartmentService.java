package com.thanhtrung.ecomicsserver.service.employee;

import com.thanhtrung.ecomicsserver.dto.employee.DepartmentRequest;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface DepartmentService extends CrudService<Long, DepartmentRequest , DepartmentResponse> {
}
