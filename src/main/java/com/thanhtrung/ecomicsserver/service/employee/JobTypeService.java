package com.thanhtrung.ecomicsserver.service.employee;

import com.thanhtrung.ecomicsserver.dto.employee.JobLevelRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface JobTypeService extends CrudService<Long, JobTypeRequest, JobTypeResponse> {
}
