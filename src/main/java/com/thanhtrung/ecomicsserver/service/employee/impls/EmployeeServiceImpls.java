package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.EmployeeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.EmployeeResponse;
import com.thanhtrung.ecomicsserver.entity.employee.Employee;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.EmployeeMapper;
import com.thanhtrung.ecomicsserver.repository.employee.EmployeeRepository;
import com.thanhtrung.ecomicsserver.service.employee.EmployeeService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class EmployeeServiceImpls implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository ;
    private EmployeeMapper employeeMapper;
    @Override
    public ListResponse<EmployeeResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.EMPLOYEE,employeeRepository,employeeMapper);
    }

    @Override
    public EmployeeResponse findById(Long id) {
        return defaultFindById(id,employeeRepository,employeeMapper, ResourceName.EMPLOYEE);
    }

    @Override
    public EmployeeResponse create(EmployeeRequest request) {
        Employee employee = employeeMapper.requestToEntity(request);
        employee.setId(GenerateNewId.generateNewId());
        return employeeMapper.entityToResponse(employeeRepository.save(employee));
    }

    @Override
    public EmployeeResponse save(Long id, EmployeeRequest request) {
        Employee jobType = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.EMPLOYEE, FieldName.ID,id));
        employeeMapper.partialUpdate(jobType, request);
        return employeeMapper.entityToResponse(employeeRepository.save(jobType));
    }

    @Override
    public void delete(Long id) {
        if (!employeeRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.EMPLOYEE, FieldName.ID, id);
        }
        employeeRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Employee> employees = ids.stream()
                .map(id -> employeeRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        employeeRepository.deleteAll(employees);
    }
}
