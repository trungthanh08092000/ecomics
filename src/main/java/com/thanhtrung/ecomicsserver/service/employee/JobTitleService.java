package com.thanhtrung.ecomicsserver.service.employee;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface JobTitleService extends CrudService<Long, JobTitleRequest, JobTitleResponse> {
}
