package com.thanhtrung.ecomicsserver.service.employee;

import com.thanhtrung.ecomicsserver.dto.employee.EmployeeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.EmployeeResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface EmployeeService extends CrudService<Long, EmployeeRequest , EmployeeResponse> {
}
