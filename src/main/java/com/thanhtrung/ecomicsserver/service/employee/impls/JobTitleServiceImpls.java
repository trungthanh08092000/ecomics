package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTitleResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobTitle;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.JobTitleMapper;
import com.thanhtrung.ecomicsserver.repository.employee.JobTitleRepository;
import com.thanhtrung.ecomicsserver.service.employee.JobTitleService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class JobTitleServiceImpls implements JobTitleService {
    @Autowired
    private JobTitleRepository jobTitleRepository;
    private JobTitleMapper jobTitleMapper;

    @Override
    public ListResponse<JobTitleResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.JOB_TITLE,jobTitleRepository,jobTitleMapper);
    }

    @Override
    public JobTitleResponse findById(Long id) {
        return defaultFindById(id,jobTitleRepository,jobTitleMapper, ResourceName.JOB_TITLE);
    }

    @Override
    public JobTitleResponse create(JobTitleRequest request) {
        JobTitle jobTitle = jobTitleMapper.requestToEntity(request);
        jobTitle.setId(GenerateNewId.generateNewId());
        return jobTitleMapper.entityToResponse(jobTitleRepository.save(jobTitle));
    }

    @Override
    public JobTitleResponse save(Long id, JobTitleRequest request) {
        JobTitle jobType = jobTitleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        jobTitleMapper.partialUpdate(jobType, request);
        return jobTitleMapper.entityToResponse(jobTitleRepository.save(jobType));
    }

    @Override
    public void delete(Long id) {
        if (!jobTitleRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.JOB_TITLE, FieldName.ID, id);
        }
        jobTitleRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<JobTitle> jobTitles = ids.stream()
                .map(id -> jobTitleRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        jobTitleRepository.deleteAll(jobTitles);
    }
}
