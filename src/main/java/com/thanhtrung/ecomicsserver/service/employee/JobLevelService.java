package com.thanhtrung.ecomicsserver.service.employee;

import com.thanhtrung.ecomicsserver.dto.employee.JobLevelRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface JobLevelService extends CrudService<Long, JobLevelRequest , JobLevelResponse> {
}
