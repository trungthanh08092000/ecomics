package com.thanhtrung.ecomicsserver.service.employee;

import com.thanhtrung.ecomicsserver.dto.employee.OfficeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface OfficeService extends CrudService<Long, OfficeRequest, OfficeResponse> {
}
