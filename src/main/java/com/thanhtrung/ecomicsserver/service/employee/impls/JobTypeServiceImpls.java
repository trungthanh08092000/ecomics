package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobTypeResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.JobTypeMapper;
import com.thanhtrung.ecomicsserver.repository.employee.JobTypeRepository;
import com.thanhtrung.ecomicsserver.service.employee.JobTypeService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class JobTypeServiceImpls implements JobTypeService {
    @Autowired
    private JobTypeRepository jobTypeRepository;
    private JobTypeMapper jobTypeMapper;


    @Override
    public ListResponse<JobTypeResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.JOB_TYPE,jobTypeRepository,jobTypeMapper);
    }

    @Override
    public JobTypeResponse findById(Long id) {
        return defaultFindById(id,jobTypeRepository,jobTypeMapper, ResourceName.JOB_TYPE);
    }

    @Override
    public JobTypeResponse create(JobTypeRequest request) {
        JobType jobType = jobTypeMapper.requestToEntity(request);
        jobType.setId(GenerateNewId.generateNewId());
        return jobTypeMapper.entityToResponse(jobTypeRepository.save(jobType));
    }

    @Override
    public JobTypeResponse save(Long id, JobTypeRequest request) {
        JobType jobType = jobTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        jobTypeMapper.partialUpdate(jobType, request);
        return jobTypeMapper.entityToResponse(jobTypeRepository.save(jobType));
    }

    @Override
    public void delete(Long id) {
        if (!jobTypeRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.JOB_TYPE, FieldName.ID, id);
        }
        jobTypeRepository.deleteById(id);

    }

    @Override
    public void delete(List<Long> ids) {
        List<JobType> jobTypes = ids.stream()
                .map(id -> jobTypeRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        jobTypeRepository.deleteAll(jobTypes);
    }
}
