package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeRequest;
import com.thanhtrung.ecomicsserver.dto.employee.OfficeResponse;
import com.thanhtrung.ecomicsserver.entity.employee.Office;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.OfficeMapper;
import com.thanhtrung.ecomicsserver.repository.employee.OfficeRepository;
import com.thanhtrung.ecomicsserver.service.employee.OfficeService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class OfficeServiceImpls implements OfficeService {
    @Autowired
    private OfficeRepository officeRepository ;
    private OfficeMapper officeMapper;
    @Override
    public ListResponse<OfficeResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.OFFICE,officeRepository,officeMapper);
    }

    @Override
    public OfficeResponse findById(Long id) {
        return defaultFindById(id,officeRepository,officeMapper, ResourceName.OFFICE);
    }

    @Override
    public OfficeResponse create(OfficeRequest request) {
        Office office = officeMapper.requestToEntity(request);
        office.setId(GenerateNewId.generateNewId());
        return officeMapper.entityToResponse(officeRepository.save(office));
    }

    @Override
    public OfficeResponse save(Long id, OfficeRequest request) {
        Office office = officeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        officeMapper.partialUpdate(office, request);
        return officeMapper.entityToResponse(officeRepository.save(office));
    }

    @Override
    public void delete(Long id) {
        if (!officeRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.OFFICE, FieldName.ID, id);
        }
        officeRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Office> offices = ids.stream()
                .map(id -> officeRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.OFFICE, FieldName.ID, id)))
                .collect(Collectors.toList());
        officeRepository.deleteAll(offices);
    }
}
