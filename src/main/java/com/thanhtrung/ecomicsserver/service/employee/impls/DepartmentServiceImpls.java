package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentRequest;
import com.thanhtrung.ecomicsserver.dto.employee.DepartmentResponse;
import com.thanhtrung.ecomicsserver.entity.employee.Department;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.DepartmentMapper;
import com.thanhtrung.ecomicsserver.repository.employee.DepartmentRepository;
import com.thanhtrung.ecomicsserver.service.employee.DepartmentService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class DepartmentServiceImpls implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository ;
    private DepartmentMapper departmentMapper;
    @Override
    public ListResponse<DepartmentResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.DEPARTMENT,departmentRepository,departmentMapper);
    }

    @Override
    public DepartmentResponse findById(Long id) {
        return defaultFindById(id,departmentRepository,departmentMapper, ResourceName.DEPARTMENT);
    }

    @Override
    public DepartmentResponse create(DepartmentRequest request) {
        Department department = departmentMapper.requestToEntity(request);
        department.setId(GenerateNewId.generateNewId());
        return departmentMapper.entityToResponse(departmentRepository.save(department));
    }

    @Override
    public DepartmentResponse save(Long id, DepartmentRequest request) {
        Department department = departmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        departmentMapper.partialUpdate(department, request);
        return departmentMapper.entityToResponse(departmentRepository.save(department));
    }

    @Override
    public void delete(Long id) {
        if (!departmentRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.DEPARTMENT, FieldName.ID, id);
        }
        departmentRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Department> departments = ids.stream()
                .map(id -> departmentRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        departmentRepository.deleteAll(departments);
    }
}
