package com.thanhtrung.ecomicsserver.service.employee.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelRequest;
import com.thanhtrung.ecomicsserver.dto.employee.JobLevelResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobLevel;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.employee.JobLevelMapper;
import com.thanhtrung.ecomicsserver.repository.employee.JobLevelRepository;
import com.thanhtrung.ecomicsserver.service.employee.JobLevelService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class JobLevelServiceImpls implements JobLevelService {
    @Autowired
    private JobLevelRepository jobLevelRepository;
    private JobLevelMapper jobLevelMapper;
    @Override
    public ListResponse<JobLevelResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.JOB_LEVEL,jobLevelRepository,jobLevelMapper);
    }

    @Override
    public JobLevelResponse findById(Long id) {
        return defaultFindById(id,jobLevelRepository,jobLevelMapper, ResourceName.JOB_LEVEL);
    }

    @Override
    public JobLevelResponse create(JobLevelRequest request) {
        JobLevel jobLevel = jobLevelMapper.requestToEntity(request);
        jobLevel.setId(GenerateNewId.generateNewId());
        return jobLevelMapper.entityToResponse(jobLevelRepository.save(jobLevel));
    }

    @Override
    public JobLevelResponse save(Long id, JobLevelRequest request) {
        JobLevel jobLevel = jobLevelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.JOB_LEVEL, FieldName.ID,id));
        jobLevelMapper.partialUpdate(jobLevel, request);
        return jobLevelMapper.entityToResponse(jobLevelRepository.save(jobLevel));
    }

    @Override
    public void delete(Long id) {
        if (!jobLevelRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.JOB_LEVEL, FieldName.ID, id);
        }
        jobLevelRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<JobLevel> jobLevels = ids.stream()
                .map(id -> jobLevelRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.JOB_LEVEL, FieldName.ID, id)))
                .collect(Collectors.toList());
        jobLevelRepository.deleteAll(jobLevels);

    }
}
