package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.BrandRequest;
import com.thanhtrung.ecomicsserver.dto.product.BrandResponse;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.product.BrandMapper;
import com.thanhtrung.ecomicsserver.repository.product.BrandRepository;
import com.thanhtrung.ecomicsserver.service.production.BrandService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class BrandServiceImpls implements BrandService {

    @Autowired
    private BrandRepository brandRepository;
    private BrandMapper brandMapper;


    @Override
    public ListResponse<BrandResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.BRAND,brandRepository,brandMapper);
    }

    @Override
    public BrandResponse findById(Long id) {
        return defaultFindById(id,brandRepository,brandMapper, ResourceName.BRAND);
    }

    @Override
    public BrandResponse create(BrandRequest request) {
        Brand brand = brandMapper.requestToEntity(request);
        brand.setId(GenerateNewId.generateNewId());
        return brandMapper.entityToResponse(brandRepository.save(brand));
    }

    @Override
    public BrandResponse save(Long id , BrandRequest request) {
        Brand brand = brandRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        brandMapper.partialUpdate(brand, request);
        return brandMapper.entityToResponse(brandRepository.save(brand));
    }

    @Override
    public void delete(Long id) {
        if (!brandRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id);
        }
        brandRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Brand> brands = ids.stream()
                .map(id -> brandRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID, id)))
                .collect(Collectors.toList());
        brandRepository.deleteAll(brands);
    }
}
