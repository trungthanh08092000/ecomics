package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.product.UnitRequest;
import com.thanhtrung.ecomicsserver.dto.product.UnitResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface UnitService extends CrudService<Long , UnitRequest,UnitResponse> {
}
