package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.SupplierRequest;
import com.thanhtrung.ecomicsserver.dto.product.SupplierResponse;
import com.thanhtrung.ecomicsserver.entity.product.Supplier;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.address.AddressMapper;
import com.thanhtrung.ecomicsserver.mapper.product.SupplierMapper;
import com.thanhtrung.ecomicsserver.repository.product.SupplierRepository;
import com.thanhtrung.ecomicsserver.service.production.SupplierService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class SupplierServiceImpls implements SupplierService {
    @Autowired
    private SupplierRepository supplierRepository ;
    private SupplierMapper supplierMapper ;
    private AddressMapper addressMapper ;
    @Override
    public ListResponse<SupplierResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.SUPPLIER,supplierRepository,supplierMapper);
    }

    @Override
    public SupplierResponse findById(Long id) {
        return defaultFindById(id,supplierRepository,supplierMapper, ResourceName.SUPPLIER);
    }

    @Override
    public SupplierResponse create(SupplierRequest request) {
        Supplier supplier = supplierMapper.requestToEntity(request);
        supplier.setId(GenerateNewId.generateNewId());
        supplier.setAddress(addressMapper.requestToEntity(request.getAddress()));
        return supplierMapper.entityToResponse(supplierRepository.save(supplier));
    }

    @Override
    public SupplierResponse save(Long id, SupplierRequest request) {
        Supplier supplier = supplierRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID,id));
        supplierMapper.partialUpdate(supplier, request);
        return supplierMapper.entityToResponse(supplierRepository.save(supplier));
    }

    @Override
    public void delete(Long id) {
        if (!supplierRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID, id);
        }
        supplierRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Supplier> suppliers = ids.stream()
                .map(id -> supplierRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID, id)))
                .collect(Collectors.toList());
        supplierRepository.deleteAll(suppliers);
    }
}
