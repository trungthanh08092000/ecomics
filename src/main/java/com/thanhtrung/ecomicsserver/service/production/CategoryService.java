package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.product.CategoryRequest;
import com.thanhtrung.ecomicsserver.dto.product.CategoryResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface CategoryService extends CrudService<Long,CategoryRequest,CategoryResponse> {
}
