package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.product.SupplierRequest;
import com.thanhtrung.ecomicsserver.dto.product.SupplierResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface SupplierService extends CrudService<Long , SupplierRequest, SupplierResponse> {
}
