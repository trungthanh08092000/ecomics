package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.CategoryRequest;
import com.thanhtrung.ecomicsserver.dto.product.CategoryResponse;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import com.thanhtrung.ecomicsserver.entity.product.Category;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.product.CategoryMapper;
import com.thanhtrung.ecomicsserver.repository.product.CategoryRepository;
import com.thanhtrung.ecomicsserver.service.production.CategoryService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class CategoryServiceImpls implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository ;
    private CategoryMapper categoryMapper;
    @Override
    public ListResponse<CategoryResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.CATEGORY,categoryRepository,categoryMapper);
    }

    @Override
    public CategoryResponse findById(Long id) {
        return defaultFindById(id,categoryRepository,categoryMapper, ResourceName.CATEGORY);
    }

    @Override
    public CategoryResponse create(CategoryRequest request) {
        Category category = categoryMapper.requestToEntity(request);
        category.setId(GenerateNewId.generateNewId());
        return categoryMapper.entityToResponse(categoryRepository.save(category));
    }

    @Override
    public CategoryResponse save(Long id, CategoryRequest request) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        categoryMapper.partialUpdate(category, request);
        return categoryMapper.entityToResponse(categoryRepository.save(category));
    }

    @Override
    public void delete(Long id) {
        if (!categoryRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.CATEGORY, FieldName.ID, id);
        }
        categoryRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Category> categories = ids.stream()
                .map(id -> categoryRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.CATEGORY, FieldName.ID, id)))
                .collect(Collectors.toList());
        categoryRepository.deleteAll(categories);
    }
}
