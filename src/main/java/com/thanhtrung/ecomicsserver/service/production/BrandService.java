package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.product.BrandRequest;
import com.thanhtrung.ecomicsserver.dto.product.BrandResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;


import java.util.List;

public interface BrandService extends CrudService<Long,BrandRequest, BrandResponse> {
}