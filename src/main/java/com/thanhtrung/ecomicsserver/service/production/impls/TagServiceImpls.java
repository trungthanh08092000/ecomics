package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.TagRequest;
import com.thanhtrung.ecomicsserver.dto.product.TagResponse;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import com.thanhtrung.ecomicsserver.entity.product.Tag;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.product.TagMapper;
import com.thanhtrung.ecomicsserver.repository.product.TagRepository;
import com.thanhtrung.ecomicsserver.service.production.TagService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class TagServiceImpls implements TagService {
    @Autowired
    private TagRepository tagRepository;
    private TagMapper tagMapper;
    @Override
    public ListResponse<TagResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.TAG,tagRepository,tagMapper);
    }

    @Override
    public TagResponse findById(Long id) {
        return defaultFindById(id,tagRepository,tagMapper, ResourceName.TAG);
    }

    @Override
    public TagResponse create(TagRequest request) {
        Tag tag = tagMapper.requestToEntity(request);
        tag.setId(GenerateNewId.generateNewId());
        return tagMapper.entityToResponse(tagRepository.save(tag));
    }

    @Override
    public TagResponse save(Long id, TagRequest request) {
        Tag tag = tagRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.BRAND, FieldName.ID,id));
        tagMapper.partialUpdate(tag, request);
        return tagMapper.entityToResponse(tagRepository.save(tag));
    }

    @Override
    public void delete(Long id) {
        if (!tagRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.TAG, FieldName.ID, id);
        }
        tagRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Tag> tags = ids.stream()
                .map(id -> tagRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.TAG, FieldName.ID, id)))
                .collect(Collectors.toList());
        tagRepository.deleteAll(tags);
    }
}
