package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.UnitRequest;
import com.thanhtrung.ecomicsserver.dto.product.UnitResponse;
import com.thanhtrung.ecomicsserver.entity.product.Supplier;
import com.thanhtrung.ecomicsserver.entity.product.Unit;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.product.UnitMapper;
import com.thanhtrung.ecomicsserver.repository.product.UnitRepository;
import com.thanhtrung.ecomicsserver.service.CrudService;
import com.thanhtrung.ecomicsserver.service.production.UnitService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class UnitServiceImpls implements UnitService {
    @Autowired
    private UnitRepository unitRepository;
    private UnitMapper unitMapper;

    @Override
    public ListResponse<UnitResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.UNIT,unitRepository,unitMapper);
    }

    @Override
    public UnitResponse findById(Long id) {
        return defaultFindById(id,unitRepository,unitMapper, ResourceName.UNIT);
    }

    @Override
    public UnitResponse create(UnitRequest request) {
        Unit unit = unitMapper.requestToEntity(request);
        unit.setId(GenerateNewId.generateNewId());
        return unitMapper.entityToResponse(unitRepository.save(unit));
    }

    @Override
    public UnitResponse save(Long id, UnitRequest request) {
        Unit unit = unitRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID,id));
        unitMapper.partialUpdate(unit, request);
        return unitMapper.entityToResponse(unitRepository.save(unit));
    }

    @Override
    public void delete(Long id) {
        if (!unitRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID, id);
        }
        unitRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Unit> units = ids.stream()
                .map(id -> unitRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.SUPPLIER, FieldName.ID, id)))
                .collect(Collectors.toList());
        unitRepository.deleteAll(units);
    }
}
