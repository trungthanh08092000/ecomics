package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.inventory.ProductInventoryResponse;
import com.thanhtrung.ecomicsserver.dto.product.ProductRequest;
import com.thanhtrung.ecomicsserver.dto.product.ProductResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface ProductService extends CrudService<Long, ProductRequest , ProductResponse> {
}
