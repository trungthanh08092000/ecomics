package com.thanhtrung.ecomicsserver.service.production.impls;

import com.thanhtrung.ecomicsserver.constant.FieldName;
import com.thanhtrung.ecomicsserver.constant.GenerateNewId;
import com.thanhtrung.ecomicsserver.constant.ResourceName;
import com.thanhtrung.ecomicsserver.constant.SearchFields;
import com.thanhtrung.ecomicsserver.dto.ListResponse;
import com.thanhtrung.ecomicsserver.dto.product.ProductRequest;
import com.thanhtrung.ecomicsserver.dto.product.ProductResponse;
import com.thanhtrung.ecomicsserver.entity.employee.JobType;
import com.thanhtrung.ecomicsserver.entity.general.Image;
import com.thanhtrung.ecomicsserver.entity.product.Brand;
import com.thanhtrung.ecomicsserver.entity.product.Product;
import com.thanhtrung.ecomicsserver.exception.ResourceNotFoundException;
import com.thanhtrung.ecomicsserver.mapper.product.ProductMapper;
import com.thanhtrung.ecomicsserver.repository.general.ImageRepository;
import com.thanhtrung.ecomicsserver.repository.product.ProductRepository;
import com.thanhtrung.ecomicsserver.service.production.ProductService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class ProductServiceImpls implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    private ProductMapper productMapper;
    private ImageRepository imageRepository;
    @Override
    public ListResponse<ProductResponse> findAll(int page, int size, String sort, String filter, String search, boolean all) {
        return defaultFindAll(page,size,sort,filter,search,all, SearchFields.PRODUCT,productRepository,productMapper);
    }

    @Override
    public ProductResponse findById(Long id) {
        return defaultFindById(id,productRepository,productMapper, ResourceName.PRODUCT);
    }

    @Override
    public ProductResponse create(ProductRequest request) {
        Product product = productMapper.requestToEntity(request);

        // Check for existing images
        List<Image> images = request.getImages().stream()
                .map(imageRequest -> imageRepository.findByName(imageRequest.getName())
                        .orElseGet(() -> imageRepository.save(new Image(
                                imageRequest.getName(),
                                imageRequest.getPath(),
                                imageRequest.getContentType(),
                                imageRequest.getSize(),
                                imageRequest.getGroup(),
                                imageRequest.getIsThumbnail(),  // Use the getter method for isThumbnail
                                imageRequest.getIsEliminated()))))  // Use the getter method for isEliminated
                .collect(Collectors.toList());

        product.setImages(images);
        return productMapper.entityToResponse(productRepository.save(product));
    }

    @Override
    public ProductResponse save(Long id, ProductRequest request) {
        Product product = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(ResourceName.PRODUCT, FieldName.ID,id));
        productMapper.partialUpdate(product, request);
        return productMapper.entityToResponse(productRepository.save(product));
    }

    @Override
    public void delete(Long id) {
        if (!productRepository.existsById(id)) {
            throw new ResourceNotFoundException(ResourceName.PRODUCT, FieldName.ID, id);
        }
        productRepository.deleteById(id);
    }

    @Override
    public void delete(List<Long> ids) {
        List<Product> products = ids.stream()
                .map(id -> productRepository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException(ResourceName.PRODUCT, FieldName.ID, id)))
                .collect(Collectors.toList());
        productRepository.deleteAll(products);    }
}
