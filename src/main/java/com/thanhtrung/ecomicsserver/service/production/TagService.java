package com.thanhtrung.ecomicsserver.service.production;

import com.thanhtrung.ecomicsserver.dto.product.ProductRequest;
import com.thanhtrung.ecomicsserver.dto.product.TagRequest;
import com.thanhtrung.ecomicsserver.dto.product.TagResponse;
import com.thanhtrung.ecomicsserver.service.CrudService;

public interface TagService extends CrudService<Long,TagRequest, TagResponse> {
}
