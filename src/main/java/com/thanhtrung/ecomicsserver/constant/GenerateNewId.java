package com.thanhtrung.ecomicsserver.constant;
import java.util.concurrent.atomic.AtomicLong;

public class GenerateNewId {
    private static final AtomicLong idGenerator = new AtomicLong();

    public static Long generateNewId() {
        return idGenerator.incrementAndGet();
    }

}
