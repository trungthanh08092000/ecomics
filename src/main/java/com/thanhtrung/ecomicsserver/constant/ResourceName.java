package com.thanhtrung.ecomicsserver.constant;

public interface ResourceName {
    String PROVINCE = "Province";
    String DISTRICT = "District";
    String WARD = "Ward";
    String ADDRESS = "Address";
    String USER = "User";
    String ROLE = "Role";
    String OFFICE = "Office";
    String DEPARTMENT = "Department";
    String JOB_TITLE = "JobTitle";
    String JOB_LEVEL = "JobLevel";
    String JOB_TYPE = "JobType";
    String EMPLOYEE = "Employee";
    String CUSTOMER_GROUP = "CustomerGroup";
    String CUSTOMER_RESOURCE = "CustomerResource";
    String CUSTOMER_STATUS = "CustomerStatus";
    String CUSTOMER = "Customer";
    String CATEGORY = "Category";
    String TAG = "Tag";
    String UNIT = "Unit";
    String SUPPLIER = "Supplier";
    String BRAND = "Brand";
    String PRODUCT = "Product";
    String IMAGE = "Image";
    String PRODUCT_INVENTORY_LIMIT = "ProductInventoryLimit";
    String WAREHOUSE = "Warehouse";
    String COUNT = "Count";
    String STORAGE_LOCATION = "StorageLocation";
    String NOTIFICATION = "Notification";
    String PROMOTION = "Promotion";
    String ROOM = "Room";
    String MESSAGE = "Message";
}
