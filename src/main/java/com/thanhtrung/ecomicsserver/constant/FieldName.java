package com.thanhtrung.ecomicsserver.constant;

public interface FieldName {
    String ID = "id";
    String SLUG = "slug";
    String USER_ID = "user_id";
    String PRODUCT_ID = "product_id";
    String USERNAME = "username";
    String CODE = "code";
}
