package com.thanhtrung.ecomicsserver.constant;

public interface SecurityConstants {
    String[] ADMIN_API_PATHS = {
            "/api/auth/info"
    };


    interface Role {
        String ADMIN = "ADMIN";

        String EMPLOYEE = "EMPLOYEE";

        String CUSTOMER = "CUSTOMER";
    }
}
